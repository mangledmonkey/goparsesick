package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// Set up global variables
var (
	sickrageID  = flag.Int("id", 0, "the Show's SickRage ID")
	sickrageURL = flag.String("url", "https://oasis.mangledmonkey.com/sickrage", "the SickRage installation URL path")
	sickrageAPI = flag.String("api", "487f79f48b908d4603dc2b1b5fd7b48f", "the SickRage API key")
	dirPath     = flag.String("d", "", "path to directory with media files")
	test        = flag.Bool("n", false, "test run without renaming")

	// Initiate global Show and seasons
	show    Show
	seasons = make(map[string]interface{})
)

// Episode has the details for each episode
type Episode struct {
	Season int
	Number int
	Title  string
}

// Show is the master struct of a show
type Show struct {
	Name     string `json:"show_name"`
	Episodes []Episode
}

// Initialize
func init() {
	flag.Parse()

	// Ensure a show ID was provided
	if *sickrageID == 0 {
		fmt.Println("Error: Missing show ID.")
		flag.Usage()
		os.Exit(1)
	}

	// If no directory provided, use current working directory
	if *dirPath == "" {
		// get current working directory
		if dir, err := filepath.Abs(filepath.Dir(os.Args[0])); err != nil {
			panic(err)
		} else {
			*dirPath = dir
		}
	}
}

// AddEpisode inserts a new episode into the list of shows
func (show *Show) AddEpisode(episode Episode) []Episode {
	show.Episodes = append(show.Episodes, episode)
	return show.Episodes
}

// getShow pulls the show name from SickRage and adds it to the Show object
func getShow() {
	showQuery := "/?cmd=show&tvdbid="

	// Make the query
	querySickRage(showQuery, &show)

}

// getEpisodes pulls the show episdoes from SickRage and adds them to the Show
func getEpisodes() {
	// var data map[string]interface{}
	seasonsQuery := "/?cmd=show.seasons&tvdbid="

	// Make the query
	querySickRage(seasonsQuery, &seasons)

	// Iterate through the show data from SickRage API
	for season, episodes := range seasons {
		// Convert season string to int
		season, err := strconv.Atoi(season)
		if err != nil {
			panic(err)
		}
		if episodes, ok := episodes.(map[string]interface{}); ok {
			for number, details := range episodes {
				// Convert number string to int
				number, err := strconv.Atoi(number)
				if err != nil {
					panic(err)
				}
				if details, ok := details.(map[string]interface{}); ok {
					// fmt.Printf("\t\tName: %s\n", details["name"])
					name := details["name"].(string)
					// Create a new Episode from SickRage data and add Episode to Show
					newEpisode := Episode{Season: season, Number: number, Title: name}
					show.AddEpisode(newEpisode)

				}
			}
		}
	}
}

// Make the API request to SickRage to pull and return JSON show data
func querySickRage(query string, data interface{}) {
	// Create raw json variable to hold result body
	var objmap map[string]*json.RawMessage
	sickragePath := *sickrageURL + "/api/" + *sickrageAPI + query + strconv.Itoa(*sickrageID)

	// Query the SickRage server
	resp, err := http.Get(sickragePath)
	if err != nil {
		fmt.Printf("ERROR: Failed to crawl \"%d\"\n", *sickrageID)
		return
	}
	defer resp.Body.Close()
	fmt.Printf("Querying SickRage at:\n\t\"%s\"\n", sickragePath)

	// Read the query results
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// Parse the query results into the objmap as json.RawMessage
	if err = json.Unmarshal(body, &objmap); err != nil {
		panic(err)
	}
	// Deposit final results into the passed interface{}
	if err = json.Unmarshal(*objmap["data"], &data); err != nil {
		panic(err)
	}
}

// Search the working directory recursively for media files by Episode.Name
// Rename found files to Plex TV file format suitable for SickRage parsing.
func renameMedia() {
	episodeCount := 0
	renameCount := 0

	fmt.Printf("Searching media files in:\n\t'%s' ...\n", *dirPath)
	for _, episode := range show.Episodes {
		episodeCount++
		// fmt.Printf("\t...s%02de%02d - %s\n", episode.Season, episode.Number, episode.Name)

		// Walk the directory passed
		err := filepath.Walk(*dirPath, func(path string, fileInfo os.FileInfo, error error) error {

			// Change title strings to Title case for comparison
			fileName := strings.ToTitle(fileInfo.Name())
			episodeTitle := strings.ToTitle(episode.Title)

			// Rename episode if filename contains episode title
			if episode.Title != "" && strings.Contains(fileName, episodeTitle) {
				base := filepath.Base(path) // get the file's basename
				dir := filepath.Dir(path)
				ext := filepath.Ext(path)
				newName := fmt.Sprintf("%s - s%02de%02d - %s", show.Name, episode.Season, episode.Number, episode.Title)

				fmt.Printf("\n  Found: %s", base)

				if base != newName+ext {
					renameCount++

					if *test != true {
						fullName := newName + ext
						fmt.Printf("\n    Renaming to: %s", fullName)
						renameTo := filepath.Join(dir, fullName)
						os.Rename(path, renameTo)
					} else {
						fmt.Printf("\n    *TEST: Renaming to: %s%s", newName, ext)

					}
					fmt.Printf(" ...done.\n")
				} else {
					fmt.Printf(" ...skipping properly named file.\n")
				}
			}

			return error

		})
		if err != nil {
			panic(err)
		}

	}

	// Report counts
	fmt.Println("--------------------------------------------------------------------------------")
	fmt.Printf("[***REPORT***] Episodes: %d, Renamed: %d\n", episodeCount, renameCount)

}

// Output flag howto
func flagUsage() {
	fmt.Printf("Usage: %s [OPTIONS] argument ...\n", os.Args[0])
	flag.PrintDefaults()
}

// Let the user know the script has completed
func done() {
	fmt.Printf("\n...wow. I guess I'm done...  Yay!!!  :D\n\n")
}

func main() {

	// Make the SickRage requests to populate the Show data
	getShow()
	getEpisodes()

	fmt.Printf("Found series name: \"%s\"\n", show.Name)
	// fmt.Println(show.Episodes)
	renameMedia()

	done()
}
