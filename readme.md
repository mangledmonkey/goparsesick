## Synopsis

Goparsesick compares the name of media files to the tiles of the episodes from your SickRage installation. If the string of the title matches the name of the episode, the script will rename the file to standard Plex Media Server format using the show name, season, episode number, and episode title. This allows SickRage to better identify and re-organize the files when scanning in the new episodes.

Example filename result:

    Show - s##e## - Title.ext

## Usage

Basic Usage:

    goparsesick -id #####

Run without writing filename changes using the `-n` flag for testing:

    goparsesick -id ##### -n

Goparsesick will use scan the current working directory unless the `-d` flag is used:

    goparsesick -id ##### -d=/media/tvshow


Flags:

    -id     the show's SickRage ID #
    -d      path to directory with media files
    -url    the SickRage installation URL path
    -api    the SickRage API key
    -n      run as test without renaming files

## TODO

 - ignore punctuation differences in title strings
 - account for episode quality in file name (SickRage usually handles this well)
 - tests
 - help flag
 - config file for common flags


## Motivation

Preparing a handful, a dozen, or hundreds of episodes in multiple seasons of a television show for scanning can be quite the headache. This simplifies the process to a quicker, more manageable task.

Goparsesick aims to solve the problem of having mis- or un-numbered episodes of a television show in a folder being scanned for processing by your custom SickRage installation.

## Installation

To install gosickrage:

    go get https://bitbucket.org/mangledmonkey/goparsesick
    go build goparsesick
    go install goparsesick